/*
zlib License

Copyright (c) 2020-2022 Rupert Carmichael

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef JG_H
#define JG_H

#ifdef __cplusplus
extern "C" {
#endif

// Semantic Versioning
#define JG_VERSION_MAJOR 1
#define JG_VERSION_MINOR 0
#define JG_VERSION_PATCH 0

// Constants
#define JG_AXES_MAX 6
#define JG_BUTTONS_MAX 32
#define JG_COORDS_MAX 3

// Enums
enum jg_loglevel {
    JG_LOG_DBG,
    JG_LOG_INF,
    JG_LOG_WRN,
    JG_LOG_ERR,
    JG_LOG_SCR
};

enum jg_pixfmt {
    JG_PIXFMT_XRGB8888,
    JG_PIXFMT_XBGR8888,
    JG_PIXFMT_RGB5551,
    JG_PIXFMT_RGB565
};

enum jg_sampfmt {
    JG_SAMPFMT_INT16,
    JG_SAMPFMT_FLT32
};

enum jg_inputtype {
    JG_INPUT_CONTROLLER,
    JG_INPUT_GUN,
    JG_INPUT_KEYBOARD,
    JG_INPUT_POINTER,
    JG_INPUT_SPINNER,
    JG_INPUT_EXTERNAL
};

enum jg_hints {
    JG_HINT_AUDIO_INTERNAL = 0x01,  // Core internally allocates audio buffer
    JG_HINT_VIDEO_INTERNAL = 0x02,  // Core internally allocates video buffer
    JG_HINT_VIDEO_ROTATE_L = 0x04,  // Screen rotated 90 degrees left
    JG_HINT_VIDEO_ROTATE_R = 0x08,  // Screen rotated 90 degrees right
    JG_HINT_MEDIA_ARCHIVED = 0x10,  // Core uses archived/compressed content
    JG_HINT_INPUT_AUDIO    = 0x20,  // Core supports audio input (microphone)
    JG_HINT_INPUT_VIDEO    = 0x40   // Core supports visual input (camera, etc)
};

// Typedefs
typedef struct _jg_coreinfo_t {
    const char *name;               // Short Name eg. "corename"
    const char *fname;              // Full Name eg. "Core Name"
    const char *version;            // Version eg. "1.0 WIP"
    const char *sys;                // Emulated system
    uint8_t numinputs;              // Number of Ports/Inputs
    uint32_t hints;                 // Hints
} jg_coreinfo_t;

typedef struct _jg_pathinfo_t {
    const char *base;               // Base user directory
    const char *core;               // Core asset directory
    const char *user;               // User asset directory
    const char *bios;               // BIOS directory
    const char *save;               // Save directory (SRAM/Memory Card)
} jg_pathinfo_t;

typedef struct _jg_fileinfo_t {
    void *data;                     // Pointer to the file data
    size_t size;                    // Size of the file data
    uint32_t crc;                   // CRC32 Checksum
    const char *md5;                // MD5 Checksum
    const char *path;               // Filesystem full path
    const char *name;               // Name of the file (without extension)
    const char *fname;              // Full name of the file (with extension)
} jg_fileinfo_t;

typedef struct _jg_videoinfo_t {
    enum jg_pixfmt pixfmt;          // Pixel Format
    unsigned wmax;                  // Max Width (X Resolution)
    unsigned hmax;                  // Max Height (Y Resolution)
    unsigned w;                     // Width
    unsigned h;                     // Height
    unsigned x;                     // X Offset
    unsigned y;                     // Y Offset
    unsigned p;                     // Pitch (Row Length)
    double aspect;                  // Aspect Ratio
    void *buf;                      // Video Buffer
} jg_videoinfo_t;

typedef struct _jg_audioinfo_t {
    enum jg_sampfmt sampfmt;        // Sample Format
    unsigned rate;                  // Sample Rate
    unsigned channels;              // Channels
    unsigned spf;                   // Samples Per Frame
    void *buf;                      // Audio Buffer
} jg_audioinfo_t;

typedef struct _jg_inputinfo_t {
    enum jg_inputtype type;         // Type of emulated input device
    int index;                      // Index/port input device is plugged into
    const char *name;               // Short name eg. "gamepad"
    const char *fname;              // Full name eg. "Game Pad"
    const char **defs;              // List of input definitions (button names)
    int numaxes;                    // Number of axes on the emulated device
    int numbuttons;                 // Number of buttons on the emulated device
} jg_inputinfo_t;

typedef struct _jg_inputstate_t {
    int16_t axis[JG_AXES_MAX];      // 6 Axes
    uint8_t button[JG_BUTTONS_MAX]; // 32 Buttons
    int32_t coord[JG_COORDS_MAX];   // 3 Coordinates
    uint8_t *keys;                  // Pointer to a core-managed array of keys
} jg_inputstate_t;

typedef struct _jg_setting_t {
    const char *name;               // Name of the setting
    const char *fname;              // Full (Friendly) name of the setting
    const char *opts;               // List of options
    const char *desc;               // Description of the setting
    int val;                        // Value of the setting
    int min;                        // Range minimum
    int max;                        // Range maximum
    int restart;                    // Restart Required (Boolean)
} jg_setting_t;

// Jolly Good API Callbacks

/**
 * Send log data to the frontend
 * @param log level
 * @param format string
 */
typedef void (*jg_cb_log_t)(int, const char *, ...);
void jg_set_cb_log(jg_cb_log_t);

/**
 * Tell the frontend how many samples to read from the audio buffer
 * @param number of samples to read
 */
typedef void (*jg_cb_audio_t)(size_t);
void jg_set_cb_audio(jg_cb_audio_t);

/**
 * Send frame time interval to the frontend
 * @param frame time interval
 */
typedef void (*jg_cb_frametime_t)(double);
void jg_set_cb_frametime(jg_cb_frametime_t);

/**
 * Send Force Feedback data to the frontend
 * @param port
 * @param strength (0.0 to 1.0)
 * @param length of time in frames
 */
typedef void (*jg_cb_rumble_t)(int, float, size_t);
void jg_set_cb_rumble(jg_cb_rumble_t);

// Jolly Good API Calls

/**
 * Initialize the core
 * @return success/fail
 */
int jg_init(void);

/**
 * Deinitialize a core
 */
void jg_deinit(void);

/**
 * Reset the system
 * @param hard or soft reset
 */
void jg_reset(int);

/**
 * Run a single frame of emulation
 */
void jg_exec_frame(void);

/**
 * Load a game
 * @return success/fail
 */
int jg_game_load(void);

/**
 * Unload a game
 * @return success/fail
 */
int jg_game_unload(void);

/**
 * Load a state by path
 * @param filename of the state to load
 */
int jg_state_load(const char *);

/**
 * Load raw state data
 * @param pointer to raw state data
 */
void jg_state_load_raw(const void*);

/**
 * Save a state by path
 * @param filename of the state to save
 */
int jg_state_save(const char *);

/**
 * Save raw state data
 * @return pointer to raw state data
 */
const void* jg_state_save_raw(void);

/**
 * Retrieve the size of state data
 * @return size of state data in bytes
 */
size_t jg_state_size(void);

/**
 * Select next disc, floppy, or other type of media
 */
void jg_media_select(void);

/**
 * Insert or remove media (disc, floppy, etc)
 */
void jg_media_insert(void);

/**
 * Clear (disable) all cheat codes
 */
void jg_cheat_clear(void);

/**
 * Set (enable) a cheat code
 * @param cheat code
 */
void jg_cheat_set(const char *);

/**
 * Rehash settings or other properties which may be modified while running
 */
void jg_rehash(void);

/**
 * Pass audio samples into the core
 * @param port number of device, if applicable (default 0)
 * @param pointer to 16-bit signed integer audio sample buffer
 * @param number of samples
 */
void jg_input_audio(int, const int16_t*, size_t);

/**
 * Return core information
 * @param system, or subsystem to use in multi-system cores
 * @return information about the core
 */
jg_coreinfo_t* jg_get_coreinfo(const char *);

/**
 * Return video information
 * @return information about video rendering
 */
jg_videoinfo_t* jg_get_videoinfo(void);

/**
 * Return audio information
 * @return information about audio
 */
jg_audioinfo_t* jg_get_audioinfo(void);

/**
 * Return audio information
 * @param port
 * @return information about an input device
 */
jg_inputinfo_t* jg_get_inputinfo(int);

/**
 * Return number of emulator-specific settings, get pointer to settings array
 * @param pointer to frontend's variable used to hold the number of settings
 * @return pointer to core's settings array
 */
jg_setting_t* jg_get_settings(size_t*);

/**
 * Set up video parameters after a game is loaded
 */
void jg_setup_video(void);

/**
 * Set up audio parameters after a game is loaded
 */
void jg_setup_audio(void);

/**
 * Pass a pointer to an input state into the emulator core
 * @param pointer to input state
 * @param index/port number
 */
void jg_set_inputstate(jg_inputstate_t*, int);

/**
 * Tell the emulator information about the game
 * @param jg_fileinfo_t
 */
void jg_set_gameinfo(jg_fileinfo_t);

/**
 * Tell the emulator information about an auxiliary file
 * @param jg_fileinfo_t
 * @param index number
 */
void jg_set_auxinfo(jg_fileinfo_t, int);

/**
 * Tell the emulator the paths for file operations
 * @param jg_pathinfo_t
 */
void jg_set_paths(jg_pathinfo_t);

#ifdef __cplusplus
}
#endif
#endif
